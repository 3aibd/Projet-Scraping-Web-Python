import scrapy
from scrapy_splash import SplashRequest
import sys
import os
import platform
import webbrowser
from colorama import init
from termcolor import cprint
from pyfiglet import figlet_format

init(strip=not sys.stdout.isatty())


class WTJspider(scrapy.Spider):
    name = "WTJ"
    array_city = []
    array_company = []
    array_title = []
    array_href = []
    list = []

    # check OS
    if platform.system() == 'Windows':
        os.system("cls")
        webbrowser.register('Edge', None)
    else:
        os.system("clear")
        webbrowser.register('Safari', None)

    cprint(figlet_format('WELCOME !'))
    print("Sector wanted :")
    try:
        # Replace whitespace by +
        jobs = input(">").replace(" ", "+")
    except KeyboardInterrupt:
        sys.exit()
    print("\n")
    print("Localisation :")
    try:
        # Replace whitespace by +
        location = input(">").replace(" ", "+")
    except KeyboardInterrupt:
        sys.exit()
    print("\n")
    print("Pages max (10 results per page, 3 pages by default)")
    try:
        page = input(">")
    except KeyboardInterrupt:
        sys.exit()
    if not page.isnumeric():
        page = 3
    cprint(figlet_format('LET\'S GO !'))
    count = int(page) - 1

    def start_requests(self):
        # create list of url with paramters and loop for page count
        self.list.append(
            "https://www.indeed.fr/jobs?q=" +
            self.jobs +
            "&l=" +
            self.location)
        for i in range(1, int(self.page)):
            self.list.append(
                "https://www.indeed.fr/jobs?q=" +
                self.jobs +
                "&l=" +
                self.location +
                "&start=" +
                str(i*10))

        # Begin scraping and call (by callback) parse function
        for uri in self.list:
            yield SplashRequest(url=uri, callback=self.parse, args={"wait": 5},
                                endpoint="render.html")

    def menu(self):
        """Print a Menu"""
        # remove shitty infos on Company name's
        for i in range(len(self.array_company)):
            self.array_href[i] = "https://www.indeed.fr" + self.array_href[i]
            if self.array_company[i][-11:] != '</a></span>':
                self.array_company[i] = self.array_company[i][30:-7].strip()
            else:
                tmp = self.array_company[i][23:].find('>')
                self.array_company[i] = self.array_company[i][
                                       tmp + 33:-11].strip()
        # Switch/Case
        print(
            'Chose display'
            '\n1: all (default)\n2: company \n3: city')
        options = {1: self.f_all,
                   2: self.f_company,
                   3: self.f_city

                   }
        try:
            choice = input(">")
        except KeyboardInterrupt:
            sys.exit()
        if not choice.isnumeric():
            choice = 1
        options[int(choice)]()

    def parse(self, response):
        """parse the indeed site with parameters

        Get the title,href,city,company of the job's website
        :param response:
        :return: callback the menu function

        """
        # get only interesting values
        title = response.xpath(
            '// *[ @ class = "jobtitle"] / a/@title').extract()
        href = response.xpath(
            '// *[ @ class = "jobtitle"] / a/@href').extract()
        city = response.xpath(
            '''// *[ contains(@class, "result")] 
            / span[contains(@class, "location")]/text()''').extract()
        company = response.xpath(
            '''// *[ contains(@class, "result")] 
            / div 
            / span[contains(@class, "company")] ''').extract()
        self.array_href.extend(href)
        self.array_city.extend(city)
        self.array_company.extend(company)
        self.array_title.extend(title)
        # call varprint function when all scraping is done
        if self.count == 0:
            self.menu()
        self.count -= 1

    def f_city(self):
        """print jobs by city

        :return: open job(s) into the browser

        """
        print("\n")
        print("Select city:")
        p_city = sorted(list(set(self.array_city)))
        for loop in p_city:
            print("{0}: {1}".format(p_city.index(loop), loop))
        try:
            choice = input(">")
        except KeyboardInterrupt:
            sys.exit()
        if not choice.isnumeric():
            self.f_city()
        if int(choice) < 0 or int(choice) > len(p_city):
            self.f_city()
        p_title = [self.array_title[i] for i in range(len(self.array_city))
                   if self.array_city[i] == p_city[int(choice)]]
        p_company = [self.array_company[i] for i in range(len(self.array_city))
                     if self.array_city[i] == p_city[int(choice)]]
        print('\n')
        print("Select a job:")
        print("999: all (default)")
        for i in range(len(p_title)):
            print("{2}: {0} / {1}".format(p_title[i], p_company[i], i))
        try:
            choice1 = input(">")
        except KeyboardInterrupt:
            sys.exit()
        if not choice1.isnumeric():
            choice1 = '999'
        for i in range(len(self.array_city)):
            if self.array_city[i] == p_city[int(choice)]:
                if choice1 != '999' and \
                        self.array_company[i] == p_company[int(choice1)] and \
                        self.array_title[i] == p_title[int(choice1)]:
                    webbrowser.open(self.array_href[i])
                elif choice1 == '999':
                    webbrowser.open(self.array_href[i])
        cprint(figlet_format('OPENING !'))

    def f_all(self):
        """print all jobs

        :return: open job(s) into the browser

        """
        print('\n')
        print("Select a job:")
        print("999: all (default)")
        for i in range(len(self.array_company)):
            print("{3}: {0} / {1} / {2}".format(self.array_title[i],
                                                self.array_city[i],
                                                self.array_company[i],
                                                i))
        try:
            choice1 = input(">")
        except KeyboardInterrupt:
            sys.exit()
        if not choice1.isnumeric():
            choice1 = '999'
        if choice1 != '999':
            webbrowser.open(self.array_href[int(choice1)])
        else:
            for i in range(len(self.array_title)):
                webbrowser.open(self.array_href[i])
        cprint(figlet_format('OPENING !'))

    def f_company(self):
        """print job(s) by company

        :return: open job(s) into the browser

        """
        print("\n")
        print("Select a company:")
        p_company = sorted(list(set(self.array_company)))
        for loop in p_company:
            print("{0}: {1}".format(p_company.index(loop), loop))
        try:
            choice = input(">")
        except KeyboardInterrupt:
            sys.exit()
        if not choice.isnumeric():
            self.f_company()
        if int(choice) < 0 or int(choice) > len(p_company):
            self.f_company()
        p_title = [self.array_title[i] for i in range(len(self.array_company))
                   if self.array_company[i] == p_company[int(choice)]]
        p_city = [self.array_city[i] for i in range(len(self.array_company))
                  if self.array_company[i] == p_company[int(choice)]]
        print('\n')
        print("Select a job:")
        print("999: all (default)")
        for i in range(len(p_title)):
            print("{2}: {0} / {1}".format(p_title[i], p_city[i], i))
        try:
            choice1 = input(">")
        except KeyboardInterrupt:
            sys.exit()
        if not choice1.isnumeric():
            choice1 = '999'
        for i in range(len(self.array_company)):
            if self.array_company[i] == p_company[int(choice)]:
                if choice1 != '999' and \
                        self.array_city[i] == p_city[int(choice1)] and \
                        self.array_title[i] == p_title[int(choice1)]:
                    webbrowser.open(self.array_href[i])
                elif choice1 == '999':
                    webbrowser.open(self.array_href[i])
        cprint(figlet_format('OPENING !'))
