# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field


class WTJItem(scrapy.Item):
    # define the fields for your item here like:
    id = scrapy.Field()
    title = scrapy.Field()
    definition = scrapy.Field()
    reference = scrapy.Field()
    date = scrapy.Field()
    duration = scrapy.Field()
    city = scrapy.Field()
