###Lancement du projet

## Installation d'un environnement Anaconda

Ce projet nécessite d'avoir un environnement anaconda. Il faut pour cela [télécharger la suite anaconda].

[télécharger la suite anaconda]: https://anaconda.org/anaconda/python


## Lancer votre environnement anaconda

Lancement du processus d'anaconda sous Windows : 
```
Anaconda prompt(WIN) 
```

Lancement du processus d'anaconda sous Mac-OS : 
```
cmd + space + taper "anaconda"
```

Création d'un environnement 
```
conda create -n PythonScraping
```

Entrer dans votre environnement de travail
```
Source activate PythonScraping
```

Installation de pip le gestionnaire de paquets pour python
```
conda install pip
```

installer scrapy et les autres packages nécessaires au projet :
```shell
pip install scrapy
pip install scrapy_splash
pip install colorama
pip install pyfiglet
```


## Execution du programme depuis un Terminal


Aller dans le repertoire du projet, taper : 
```
scrapy crawl WTJ
```

![demarrage prog](demarrageProg.png)
```
Taper un mot, exemple : "big data"
```

Il ne reste plus qu'à suivre les instructions de l'interface utilisateur.

Remarque : Le programme doit être redémarré après chaque recherche.